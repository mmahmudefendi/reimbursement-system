<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController as Employee;
use App\Http\Controllers\ReimbursementController as Reimbursement;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});

Route::middleware(['web'])->group(function () {
    Route::prefix('employees')->middleware(['directorRole'])->group(function () {
        Route::get('', [Employee::class, 'index'])->name('employee');
        Route::get('count', [Employee::class, 'count'])->name('employee_count');
        Route::get('datatable', [Employee::class, 'datatable'])->name('employee_datatable');
        Route::post('', [Employee::class, 'store'])->name('employee_create');
        Route::put('{id}', [Employee::class, 'update'])->name('employee_update');
        Route::delete('{id}', [Employee::class, 'destroy'])->name('employee_delete');
    });
    
    Route::prefix('reimbursements')->group(function () {
        Route::get('', [Reimbursement::class, 'index'])->name('reimbursement');
        Route::get('count', [Reimbursement::class, 'count'])->name('reimbursement_count');
        Route::get('datatable', [Reimbursement::class, 'datatable'])->name('reimbursement_datatable');
        Route::delete('{id}', [Reimbursement::class, 'destroy'])->name('reimbursement_delete');
        Route::patch('{id}/{status}', [Reimbursement::class, 'submission'])->name('reimbursement_submission');

        Route::middleware('staffRole')->group(function () {
            Route::post('', [Reimbursement::class, 'store'])->name('reimbursement_create');
            Route::post('{id}/update', [Reimbursement::class, 'update'])->name('reimbursement_update');
        });
    });
});

