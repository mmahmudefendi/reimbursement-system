<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('ALTER TABLE roles DISABLE TRIGGER ALL;');
        DB::table('roles')->truncate();
        
        Role::create(['name' => "Director"]);
        Role::create(['name' => "Finance"]);
        Role::create(['name' => "Staff"]);

        DB::statement('ALTER TABLE roles ENABLE TRIGGER ALL;');

    }
}
