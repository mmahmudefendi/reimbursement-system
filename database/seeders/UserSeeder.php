<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'role_id' => 1,
            'nip' => 1234,
            'name' => 'Doni',
            'email' => 'doni@kasirpintar.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make(123456)
        ]);
        User::create([
            'role_id' => 2,
            'nip' => 1235,
            'name' => 'Dono',
            'email' => 'dono@kasirpintar.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make(123456)
        ]);
        User::create([
            'role_id' => 3,
            'nip' => 1236,
            'name' => 'Dona',
            'email' => 'dona@kasirpintar.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make(123456)
        ]);
    }
}
