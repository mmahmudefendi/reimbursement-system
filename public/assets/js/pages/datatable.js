'use strict'

// Class definition
let KTDatatablesServerSide = (function () {
  // Shared variables
  let table
  let dt
  const datatableID = '#datatable-events'
  let action_url = $(datatableID).data('action')

  // Private functions
  let initDatatable = function () {
    dt = $(datatableID).DataTable({
      searchDelay: 500,
      processing: true,
      serverSide: true,
      order: [[0, 'desc']],
      // stateSave: true,
      ajax: {
        method: 'POST',
        url: $(datatableID).data('endpoint'),
        data: function (d) {
          d.type = $('#btn_show_trash').data('show')
          let params = $(datatableID).data('params')
          if (params) {
            $.extend(d, params)
          }
        },
        dataSrc: function (response) {
          $('#total').text(`${response.beautyRecordsTotal} items`)
          return response.data
        }
      },
      columns: [
        { data: 'id' },
        { data: 'name' },
        { data: 'beauty_date' },
        { data: 'profile_student_count' },
        { data: 'status' },
        // { data: 'for_university' },
        // { data: 'exhibitor_count' }
      ],
      columnDefs: [
        {
          targets: 1,
          orderable: true,
          render: function (data, type, row) {
            return `
              <div class='gap-2'>
                <div class='d-flex'>
                  <a href='${action_url}/${row.id}' class='text-gray-800 text-hover-primary mb-1'>${data}</a>
                </div>
              </div>`
          }
        },
        {
          targets: 2,
          className: 'text-center',
          orderable: false
        },
        {
          targets: 3,
          className: 'text-center'
        },
        {
          targets: 4,
          className: 'text-center',
          render: function (data, type, row) {
            let status = {
              publish: 'success',
              draft: 'primary'
            }
            return `<div class='badge badge-light-${status[data]} fw-bold text_change_status' data-id='${row.id}'>${data.toUpperCase()}</div>`
          }
        },
        // {
        //   targets: 5,
        //   className: 'text-center',
        //   render: function (data, type, row) {
        //     let status = {
        //       0: svg.check_inactive,
        //       1: svg.check_active
        //     }

        //     return status[data]
        //   }
        // },
        // {
        //   targets: 6,
        //   className: 'text-center'
        // },
        {
          targets: 5,
          className: 'text-center',
          render: function (data, type, row) {
            let role = $(datatableID).data('role')
            let accessEvent = $(datatableID).data('role-can-modify') ?? [1, 6, 9, 10]
            let can_modify = accessEvent.includes(role) ? true : false
            // let can_modify = role == 1 || role == 6 || role == 9 ? true : false

            let checked = row.status === 'publish' ? 'checked' : ''

            let switchButton = `
              <div data-bs-toggle='tooltip' data-bs-placement='top' title='Change events status' class='form-check form-switch form-check-custom form-check-success form-check-solid'>
                <input class='form-check-input' id='switch_change_status' data-status='${row.status}' ${checked} type='checkbox' value='${row.id}' />
              </div>
            `

            let pinnedButton = ''
            if (row.sort) {
              pinnedButton = `
                <button class='btn btn-icon svg-icon svg-icon-1 svg-icon-primary switch_pin_status' data-id='${row.id}' data-pin='${row.sort}'>
                    ${svg.pinned}
                </button>
              `
            } else {
              pinnedButton = `
                <button class='btn btn-icon svg-icon svg-icon-1 svg-icon-secondary switch_pin_status' data-id='${row.id}' data-pin='${row.sort}'>
                    ${svg.unpinned}
                </button>
              `
            }

            let participantButton = `
              <div class='menu-item px-3'>
                <a href='${action_url}/participant/${row.id}' class='menu-link px-3'>
                  <span class='menu-icon'>
                      ${svg.user}
                  </span>
                  Participants
                </a>
              </div>
            `

            let exhibitorButton = ''
            if (row.for_university) {
              exhibitorButton = `
                <div class='menu-item px-3'>
                  <a href='${action_url}/exhibitors/${row.id}' class='menu-link px-3'>
                    <span class='menu-icon'>
                        ${svg.users}
                    </span>
                    Exhibitors
                  </a>
                </div>
              `
            }

            let downloadButton = ''
            if (row.profile_student_count) {
              downloadButton = `
                <div class='menu-item px-3'>
                  <a href='${action_url}/download/${row.id}' class='menu-link px-3'>
                    <span class='menu-icon'>
                        ${svg.cloud_download}
                    </span>
                    Download
                  </a>
                </div>
              `
            }

            let blastButton = `
              <div class='menu-item px-3'>
                <a href='${action_url}/blast/${row.id}' class='menu-link px-3'>
                  <span class='menu-icon'>
                      ${svg.send}
                  </span>
                  Blast
                </a>
              </div>
            `

            let editButton = `
              <div class='menu-item px-3'>
                <a href='${action_url}/${row.id}' class='menu-link px-3'>
                  <span class='menu-icon'>
                      ${svg.edit}
                  </span>
                  Edit
                </a>
              </div>
            `

            let deleteButton = `
              <div class='menu-item px-3'>
                <a href='#' class='menu-link px-3 btn-trash' data-id='${row.id}' data-name='${row.name}'>
                  <span class='menu-icon'>
                      ${svg.delete}
                  </span>
                  Delete
                </a>
              </div>
            `

            let actionList = ''
            if (can_modify) {
              actionList = `
                ${switchButton}
                ${pinnedButton}
                <a href='#' data-kt-menu-trigger='click' data-kt-menu-placement='bottom-end'>
                    ${svg.elipsis_h}
                </a>
                <div class='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-150px py-4' data-kt-menu='true'>
                  <div class='menu-item px-3'>
                    <a href='${row.single_url}' target='_blank' class='menu-link px-3'>
                      <span class='menu-icon'>
                          ${svg.view}
                      </span>
                      Preview
                    </a>
                  </div>
                  ${participantButton}
                  ${exhibitorButton}
                  ${downloadButton}
                  ${blastButton}
                  ${editButton}
                  ${deleteButton}
                </div>
              `
            }

            if (role != 1 && role != 6 && row.for_university) {
              actionList = `
                <a href='#' data-kt-menu-trigger='click' data-kt-menu-placement='bottom-end'>
                    ${svg.elipsis_h}
                </a>
                <div class='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-150px py-4' data-kt-menu='true'>
                  <div class='menu-item px-3'>
                    <a href='${row.single_url}' target='_blank' class='menu-link px-3'>
                      <span class='menu-icon'>
                          ${svg.view}
                      </span>
                      Preview
                    </a>
                  </div>
                  ${exhibitorButton}
                </div>
              `
            }

            let show = $('#btn_show_trash').data('show')
            if (show !== 'all') {
              actionList = `
                <a href='#' data-kt-menu-trigger='click' data-kt-menu-placement='bottom-end'>
                    ${svg.elipsis_h}
                </a>
                <div class='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-150px py-4' data-kt-menu='true'>
                  <div class='menu-item px-3'>
                    <a href='javascript:' class='menu-link px-3 btn-restore' data-id='${row.id}' data-name='${row.name}'>
                      <span class='menu-icon'>
                          ${svg.restore}
                      </span>
                      Restore
                    </a>
                  </div>
                </div>
              `
            }

            return `
              <div class='d-flex flex-row justify-content-center align-items-center gap-1'>
                ${actionList}
              </div>
            `
          }
        }
      ]
    })

    table = dt.$
    // Re-init functions on every table re-draw -- more info: https://datatables.net/reference/event/draw
    dt.on('draw', function () {
      KTMenu.createInstances()
    })
  }

  // Search Datatable --- official docs reference: https://datatables.net/reference/api/search()
  let handleSearchDatatable = function () {
    const filterSearch = document.querySelector(
      '[data-kt-user-table-filter="search"]'
    )
    if (filterSearch !== null) {
      filterSearch.addEventListener('keyup', function (e) {
        datatableSearch(dt, e.target.value)
      })
    }
  }

  // Filter Datatable
  let handleFilterDatatable = () => {
    $('#btn_show_trash').on('click', function () {
      setDatatableTitle('Events', $(this))
      dt.draw()
    })

    $('#filter_button').on('click', function () {
      let status = $('#filter_status').val()

      let params = {}

      if (status && status !== 'all') {
        params.status = status
      }

      $(datatableID).data('params', params)
      dt.draw()
    })
  }

  // Reset Filter Datatable
  let handleResetFilterDatatable = () => {
    $('#filter_reset').on('click', function () {
      $(datatableID).data('params', null)
      dt.draw()
    })
  }

  // Handle submit New Event
  let handleSubmitNewEvent = () => {
    $('#select-type').select2({ placeholder: 'Pick Type', dropdownParent: '#fg-select-type' })
    $('#event_date').daterangepicker({
      autoUpdateInput: false,
      showDropdowns: true,
      timePicker: true,
      timePicker24Hour: true,
      // startDate: moment(),
      // endDate: moment().endOf('year'),
      minDate: moment(),
      opens: "center",
      drops: "up",
      locale: {
        format: 'DD-MM-YYYY HH:mm'
      }
    })
    $('#event_date').on('apply.daterangepicker', function (ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY HH:mm') + ' to ' + picker.endDate.format('DD-MM-YYYY HH:mm'))
    })
    $('#select-type').on('change', function () {
      $('.type-wrapper').hide()
      if ($(this).val() == 'virtual-edu-fair') {
        $('#wrapper-eid').show()
      }
      if ($(this).val() == 'webinar') {
        $('#wrapper-webinar').show()
      }
      if ($(this).val() == 'offline') {
        $('#wrapper-offline').show()
      }
    })
    $('#modal_new_event').on('hidden.bs.modal', function () {
      $('#form_new_event')[0].reset()
      $('.type-wrapper').hide()
    })
    // Begin Submit New Form
    $(document).on('submit', '#form_new_event', function (event) {
      event.preventDefault()
      let action = $(this).attr('action')
      let submitButton = $('.btn-save')
      toggleSubmitButton(submitButton, 'on')
      // Do Ajax
      $.ajax({
        url: action,
        type: 'POST',
        data: $(this).serialize(),
        success: function (response) {
          $('#modal_new_event').modal('hide')
          toggleSubmitButton(submitButton, 'off')
          dt.draw()
          makeNotifyToastr(
            response.header,
            response.message,
            response.status)
          if (response.status == 'success') {
            setTimeout(function () {
              window.location.href = response.data
            }, 500)
          }
        }
      })
    })
    // End Submit New Form
  }

  // Handle change status
  let handleChangeStatus = () => {
    $(document).on('click', '#switch_change_status', function (e) {
      let id = $(this).val()
      let action = `${action_url}/update-status`
      let status = $(this).data('status')

      //Change status text
      let label = { publish: 'success', draft: 'primary' }
      let new_label = status == 'publish' ? 'draft' : 'publish'
      $(this).data('status', new_label)

      $.ajax({
        url: action,
        type: 'POST',
        data: {
          id: id
        },
        success: function (response) {
          // dt.draw()
          $(`.text_change_status[data-id=${id}]`)
            .removeClass(`badge-light-${label[status]}`)
            .addClass(`badge-light-${label[new_label]}`)
            .html(new_label.toUpperCase())

          makeNotifyToastr(response.header, response.message, response.status)
        }
      })
      $(datatableID).data('action')
    })
  }

  // Handle change status
  let handleChangePinStatus = () => {
    $(document).on('click', '.switch_pin_status', function (e) {
      let id = $(this).data('id')
      let action = `${action_url}/pin`
      let pin = $(this).data('pin')

      $.ajax({
        url: action,
        type: 'POST',
        data: {
          id: id
        },
        success: function (response) {
          // dt.draw()

          //Change pin status text
          let pin_label = { 1: 'primary', 0: 'secondary' }
          let new_pin_label = pin == 1 ? 0 : 1
          let new_pin_icon = { 1: svg.pinned, 0: svg.unpinned }

          $(`.switch_pin_status[data-id=${id}]`)
            .data('pin', new_pin_label)
            .removeClass(`svg-icon-${pin_label[pin]}`)
            .addClass(`svg-icon-${pin_label[new_pin_label]}`)
            .html(new_pin_icon[new_pin_label])

          makeNotifyToastr(response.header, response.message, response.status)
        }
      })
      $(datatableID).data('action')
    })
  }

  // Handle delete
  let handleDeleteData = () => {
    $(document).on('click', '.btn-trash', function (e) {
      e.preventDefault()
      let name = $(this).attr('data-name')
      let id = $(this).attr('data-id')
      let action = `${action_url}/${id}`

      Swal.fire({
        title: 'Are you sure?',
        html: 'Move to trash Event : <strong>' + name + '</strong>',
        icon: 'warning',
        allowOutsideClick: false,
        buttonsStyling: false,
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonText: 'Yes, Move to trash',
        cancelButtonText: 'No, return',
        customClass: {
          confirmButton: 'btn btn-primary',
          cancelButton: 'btn btn-active-light'
        }
      }).then(function (result) {
        if (result.value) {
          $.ajax({
            url: action,
            type: 'DELETE',
            data: {
              type: 'soft-delete'
            },
            success: function (response) {
              dt.draw()
              makeNotifyToastr(
                response.header,
                response.message,
                response.status
              )
            }
          })
        }
      })
    })
  }

  // Handle restore
  let handleRestoreData = () => {
    $(document).on('click', '.btn-restore', function (e) {
      e.preventDefault()
      let name = $(this).attr('data-name')
      let id = $(this).attr('data-id')
      let action = `${action_url}/restore/${id}`

      Swal.fire({
        title: 'Are you sure?',
        html: 'Restore Event : <strong>' + name + '</strong>',
        icon: 'warning',
        allowOutsideClick: false,
        buttonsStyling: false,
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonText: 'Yes, Restore',
        cancelButtonText: 'No, return',
        customClass: {
          confirmButton: 'btn btn-primary',
          cancelButton: 'btn btn-active-light'
        }
      }).then(function (result) {
        if (result.value) {
          $.ajax({
            url: action,
            type: 'GET',
            success: function (response) {
              dt.draw()
              makeNotifyToastr(
                response.header,
                response.message,
                response.status
              )
            }
          })
        }
      })
    })
  }

  // Handle permanent delete
  // let handlePermanentDeleteData = () => {
  //   $(document).on('click', '.btn-delete', function (e) {
  //     e.preventDefault()
  //     let title = $(this).attr('data-title')
  //     let id = $(this).attr('data-id')
  //     let action = `${action_url}/${id}`

  //     Swal.fire({
  //       title: 'Are you sure?',
  //       html: 'Permanent Delete Post : <strong>' + title + '</strong>',
  //       icon: 'warning',
  //       allowOutsideClick: false,
  //       buttonsStyling: false,
  //       showCancelButton: true,
  //       reverseButtons: true,
  //       confirmButtonText: 'Yes, Permanent Delete!',
  //       cancelButtonText: 'No, return',
  //       customClass: {
  //         confirmButton: 'btn btn-primary',
  //         cancelButton: 'btn btn-active-light'
  //       }
  //     }).then(function (result) {
  //       if (result.value) {
  //         $.ajax({
  //           url: action,
  //           type: 'DELETE',
  //           data: {
  //             type: 'permanent-delete'
  //           },
  //           success: function (response) {
  //             dt.draw()
  //             makeNotifyToastr(
  //               response.header,
  //               response.message,
  //               response.status
  //             )
  //           }
  //         })
  //       }
  //     })
  //   })
  // }

  // Public methods
  return {
    init: function () {
      initDatatable()
      handleSearchDatatable()
      handleFilterDatatable()
      handleResetFilterDatatable()
      handleDeleteData()
      handleChangeStatus()
      handleChangePinStatus()
      handleRestoreData()
      // handlePermanentDeleteData()
      handleSubmitNewEvent()
    }
  }
})()

// On document ready
KTUtil.onDOMContentLoaded(function () {
  KTDatatablesServerSide.init()
})
