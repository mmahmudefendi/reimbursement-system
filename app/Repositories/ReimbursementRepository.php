<?php

namespace App\Repositories;

use App\Http\Resources\ReimbursementCollection;
use App\Models\Reimbursement;
use App\Services\DataTableService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ReimbursementRepository
{
  protected $dataTableService;

  public function __construct(DataTableService $dataTableService)
  {
      $this->dataTableService = $dataTableService;
  }
  public function count() {
    return Reimbursement::count();
  }

  public function datatable($request) {
    $query = Reimbursement::with('user');

    $sortBy = "id";
    $sortDir = "desc";

    if (isset($request['query']['generalSearch'])) {
    $q = $request['query']['generalSearch'];
        $query->where('name', 'LIKE', "%" . $q . "%");
        $query->orWhere('status', 'LIKE', "%" . $q . "%");
        $query->orWhere('description', 'LIKE', "%" . $q . "%");
    }
    
    $results = $this->dataTableService->setDatatable($request, $query, $sortBy, $sortDir);
    return response()->json(new ReimbursementCollection($results));
  }

  public function store($request) {
    $reimbursement = Reimbursement::create([
      'date' => $request->date,
      'name' => $request->name,
      'description' => $request->description,
      'user_id' => Auth::user()->id,
    ]);

    if ($request->hasFile('file')) {
      $imageName = time().'.'.$request->file->extension();
      $location = 'files/' . $reimbursement->id;
      $uploadedImage = $request->file->move(public_path($location), $imageName);
      $imagePath = $location . '/' . $imageName;
      $reimbursement->file = $imagePath;
      $reimbursement->update();
    }

    return response()->json([
      'status' => 'success',
      'message' => 'success add reimbursement'
    ], 201);
  }

  public function update($id, $request) {
    $reimbursement = Reimbursement::findOrFail($id);
    $reimbursement->date = $request->date;
    $reimbursement->name = $request->name;
    $reimbursement->description = $request->description;
    $reimbursement->update();

    if ($request->hasFile('file')) {
      $imageName = time().'.'.$request->file->extension();
      $location = 'files/' . $reimbursement->id;
      $uploadedImage = $request->file->move(public_path($location), $imageName);
      $imagePath = $location . '/' . $imageName;
      $reimbursement->file = $imagePath;
      $reimbursement->update();
    }

    return response()->json([
      'status' => 'success',
      'message' => 'success update reimbursement'
    ]);
  }

  public function destroy($id) {
    $reimbursement = Reimbursement::findOrFail($id);
    $reimbursement->delete();

    return response()->json([
      'status' => 'success',
      'message' => 'delete success'
    ]);
  }

  public function submission($id, $status) {
    $reimbursement = Reimbursement::findOrFail($id);
    $reimbursement->status = $status;
    $reimbursement->update();

    return response()->json([
      'status' => 'success',
      'message' => 'change status success'
    ]);
  }
}