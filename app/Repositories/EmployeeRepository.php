<?php

namespace App\Repositories;

use App\Http\Resources\UserCollection;
use App\Models\User;
use App\Services\DataTableService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EmployeeRepository
{
  protected $dataTableService;

  public function __construct(DataTableService $dataTableService)
  {
      $this->dataTableService = $dataTableService;
  }
  public function count() {
    return User::count();
  }

  public function datatable($request) {
    $query = User::with('role');

    $sortBy = "id";
    $sortDir = "desc";

    if (isset($request['query']['generalSearch'])) {
    $q = $request['query']['generalSearch'];
        $query->where('name', 'LIKE', "%" . $q . "%");
        $query->orWhere('email', 'LIKE', "%" . $q . "%");
        $query->orWhere('nip', 'LIKE', "%" . $q . "%");
    }
    
    $results = $this->dataTableService->setDatatable($request, $query, $sortBy, $sortDir);
    return response()->json(new UserCollection($results));
  }

  public function store($request) {
    $user = User::create([
      'role_id' => $request->role_id,
      'nip' => $request->nip,
      'name' => $request->name,
      'email' => $request->email,
      'password' => Hash::make($request->password),
    ]);

    return response()->json([
      'status' => 'success',
      'message' => 'success add account employee'
    ], 201);
  }

  public function destroy($id) {
    $user = User::where('id', '<>', Auth::user()->id)->findOrFail($id);
    $user->delete();

    return response()->json([
      'status' => 'success',
      'message' => 'delete success'
    ]);
  }

  public function update($id, $request) {
    $user = User::findOrFail($id);
    $user->nip = $request->nip;
    $user->name = $request->name;
    $user->email = $request->email;
    $user->role_id = $request->role_id;
    $user->update();

    return response()->json([
      'status' => 'success',
      'message' => 'update employee success'
    ]);
  }
}