<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository
{
  public function role() {
    return Role::get();
  }
}