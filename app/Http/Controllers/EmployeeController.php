<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCollection;
use App\Models\User;
use App\Repositories\EmployeeRepository;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EmployeeController extends Controller
{

    private $employeeRepository;
    private $roleRepository;

    public function __construct(EmployeeRepository $employeeRepository, RoleRepository $roleRepository)
    {
        $this->employeeRepository = $employeeRepository;
        $this->roleRepository = $roleRepository;
    }

    public function index()
    {
        $roles = $this->roleRepository->role();
        return view('app.employees.index', compact('roles'));
    }

    public function count()
    {
      return $this->employeeRepository->count();
    }

    public function datatable(Request $request)
    {
        return $this->employeeRepository->datatable($request);    
    }

    public function store(Request $request)
    {
        return $this->employeeRepository->store($request);    
    }

    public function update($id, Request $request)
    {
        return $this->employeeRepository->update($id, $request);    
    }

    public function destroy($id)
    {
        return $this->employeeRepository->destroy($id);    
    }
}
