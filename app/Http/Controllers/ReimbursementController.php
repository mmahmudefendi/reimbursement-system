<?php

namespace App\Http\Controllers;

use App\Repositories\ReimbursementRepository;
use Illuminate\Http\Request;

class ReimbursementController extends Controller
{
    private $reimbursementRepository;

    public function __construct(ReimbursementRepository $reimbursementRepository)
    {
        $this->reimbursementRepository = $reimbursementRepository;
    }

    public function index()
    {
        return view('app.reimbursements.index');
    }

    public function count()
    {
      return $this->reimbursementRepository->count();
    }

    public function datatable(Request $request)
    {
        return $this->reimbursementRepository->datatable($request);    
    }

    public function store(Request $request)
    {
        return $this->reimbursementRepository->store($request);    
    }

    public function update($id, Request $request)
    {
        return $this->reimbursementRepository->update($id, $request);    
    }

    public function destroy($id)
    {
        return $this->reimbursementRepository->destroy($id);    
    }

    public function submission($id, $status)
    {
        return $this->reimbursementRepository->submission($id, $status);    
    }
}
