<?php

namespace App\Services;

class DataTableService
{
  public function setDatatable($request, $query, $sortBy, $sortDir)
  {
    // For KTDatatable
    $page = 1;
    $per_page = 10;

    // Define the default order
    $order_field = $sortBy;
    $order_sort = $sortDir;

    // Get the request parameters
    $params = $request->all();

    // Set the current page
    if (isset($params['pagination']['page'])) {
        $page = $params['pagination']['page'];
    }

    // Set the number of items
    if (isset($params['pagination']['perpage'])) {
        $per_page = $params['pagination']['perpage'];
    }

    // Set the sort order and field
    if (isset($params['sort']['field'])) {
        $order_field = $params['sort']['field'];
        $order_sort = $params['sort']['sort'];
    }

    // Get how many items there should be
    $total = $query->limit($per_page)->count();

    // Get the items defined by the parameters
    $results = $query->skip(($page - 1) * $per_page)
        ->take($per_page)->orderBy($order_field, $order_sort)
        ->get();

    $request->meta = [
        "page" => (int)$page,
        "pages" => ceil($total / $per_page),
        "perpage" => $per_page,
        "total" => $total,
        "sort" => $order_sort,
        "field" => $order_field
    ];

    return $results;
  }
}