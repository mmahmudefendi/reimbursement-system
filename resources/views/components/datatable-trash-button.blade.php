@props(['id' => 'btn_show_trash', 'dt' => 'datatable-title'])

<button type="button" class="btn btn-sm btn-light me-3" id="{{ $id }}" data-show="all" data-datatable="#{{ $dt }}" data-show-trash-btn="#{{ $id . '_label_trash' }}" data-show-all-btn="#{{ $id . '_label_all' }}">
  <span id="{{ $id . '_label_trash' }}">
    <x-svg-icon type="delete" /> Show Trash
  </span>
  <span id="{{ $id . '_label_all' }}" class="d-none">
    <x-svg-icon type="stack" /> Show All
  </span>
</button>
