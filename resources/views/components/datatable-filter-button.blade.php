<div class="d-flex justify-content-end">
  <button type="reset" class="btn btn-light btn-active-light-primary fw-semibold me-2 px-6" data-kt-menu-dismiss="true" id="filter_reset">Reset</button>
  <button type="button" class="btn btn-primary fw-semibold px-6" data-kt-menu-dismiss="true" id="filter_button">Apply</button>
</div>
