@props(['statuses' => ['publish', 'draft']])
@php
  $statuses = array_merge(['all'], $statuses);
@endphp
<div class="mb-10">
  <label class="form-label fs-6 fw-semibold">Status:</label>
  <select class="form-select form-select-solid fw-bold" data-kt-select2="true" data-placeholder="Select status" id="filter_status">
    @foreach ($statuses as $item)
      <option value="{{ $item }}">{{ ucwords($item) }}</option>
    @endforeach
  </select>
</div>
