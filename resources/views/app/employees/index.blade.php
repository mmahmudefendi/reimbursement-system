@extends('layouts.app-admin')
@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <div class="d-flex align-items-center flex-wrap mr-2">
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Data Employee Accounts</h5>
        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
        <div id="total"></div>
      </div>
      <div class="d-flex align-items-center">
        {{-- <a href="" class="btn btn-light-primary font-weight-bolder btn-sm">New Account Employee</a> --}}
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal_new_event">
          New Account Employee
        </button>
      </div>
    </div>
  </div>
  <div class="d-flex flex-column-fluid">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card card-custom">
            <div class="card-header card-header-tabs-line">
              <div class="card-title">
              </div>
              <div class="card-toolbar">
                <ul class="nav nav-tabs nav-bold nav-light-danger nav-tabs-line">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab_all">
                      <span class="nav-icon"><i class="flaticon2-soft-icons"></i></span>
                      <span class="nav-text">All</span>
                    </a>
                  </li>
                  {{-- <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab_trash">
                      <span class="nav-icon"><i class="flaticon2-trash"></i></span>
                      <span class="nav-text">Trash</span>
                    </a>
                  </li> --}}
                </ul>
              </div>
            </div>
            <div class="card-body">
              <div class="tab-content">
                <div class="tab-pane fade show active" id="tab_all" role="tabpanel" aria-labelledby="tab_all">
                  <!--begin::Search Form-->
                  <div class="mb-7">
                    <div class="row align-items-center">
                      <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center justify-content-right">
                          <div class="col-md-4 my-2 my-md-0">
                            <div class="input-icon">
                              <input type="text" class="form-control" placeholder="Cari..."
                                id="dt_all_search_query" />
                              <span>
                                <i class="flaticon2-search-1 text-muted"></i>
                              </span>
                            </div>
                          </div>
                    

                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end::Search Form-->
                  <div id="dt_all" class="datatable datatable-bordered datatable-head-custom"></div>
                </div>
                <div class="tab-pane fade" id="tab_trash" role="tabpanel" aria-labelledby="tab_trash">
                  <div class="d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <div class="d-flex align-items-center flex-wrap mr-2">
                      <div class="font-size-lg">Trashed Employee</div>
                    </div>
                    <div class="d-flex align-items-center">
                      <div class="font-size-lg mr-4">Filter</div>
                      <div class="input-icon">
                        <input type="text" class="form-control" placeholder="Search..." id="dt_trash_search_query">
                        <span>
                          <i class="flaticon2-search-1 text-muted"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div id="dt_trash" class="datatable datatable-bordered datatable-head-custom"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- Begin Modal New Event --}}
<div class="modal fade" id="modal_new_event" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form class="form" id="form_new_event">
        <div class="modal-header">
          <h5 class="modal-title">New Account Employee</h5>
          <button type="button" class="btn btn-icon close" data-dismiss="modal" aria-label="Close">
            <x-svg-icon type="cross" />
          </button>
        </div>
        <div class="modal-body">
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">NIP<span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="nip" placeholder="NIP" value="" required autofocus />
          </div>
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">Name<span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="name" placeholder="Name" value="" required autofocus />
          </div>
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">Email<span class="text-danger">*</span></label>
            <input type="email" class="form-control" name="email" placeholder="Email" value="" required autofocus />
          </div>
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">Password<span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="password" placeholder="Password" value="" required autofocus />
          </div>
          <div class="fv-row" id="fg-select-type">
            <label class="required fw-semibold fs-6 mb-2">Role<span class="text-danger">*</span></label>
            <select name="role_id" class="form-control" id="select-type" placeholder="Chose Type" required>
              <option value="">Choose Role</option>
              @foreach ($roles as $role)
                <option value="{{ $role['id'] }}">{{ $role['name'] }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light-primary fw-bold" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary fw-bold" id="btn-save-new-employee">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- End Modal New Event --}}

{{-- Begin Modal New Event --}}
<div class="modal fade" id="modal_edit_employee" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form class="form" id="form_edit_employee">
        <div class="modal-header">
          <h5 class="modal-title">Edit Account Employee</h5>
          <button type="button" class="btn btn-icon close" data-dismiss="modal" aria-label="Close">
            <x-svg-icon type="cross" />
          </button>
        </div>
        <div class="modal-body">
          <div class="fv-row mb-5">
            <input type="hidden" name="id" id="edit_id" />
            <label class="required fw-semibold fs-6 mb-2">NIP<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="data-nip" name="nip" placeholder="NIP" value="" required autofocus />
          </div>
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">Name<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="data-name" name="name" placeholder="Name" value="" required autofocus />
          </div>
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">Email<span class="text-danger">*</span></label>
            <input type="email" class="form-control" id="data-email" name="email" placeholder="Email" value="" required autofocus />
          </div>
          <div class="fv-row" id="fg-select-type">
            <label class="required fw-semibold fs-6 mb-2">Role<span class="text-danger">*</span></label>
            <select name="role_id" class="form-control" id="data-role" placeholder="Chose Type" required>
              <option value="">Choose Role</option>
              @foreach ($roles as $role)
                <option value="{{ $role['id'] }}">{{ $role['name'] }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light-primary fw-bold" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary fw-bold" id="btn-save-new-employee">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- End Modal New Event --}}

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
  $(document).ready(function(){
    let user_login_role_id = "{{ Auth::user()->role_id }}";
    // let arr_role_id = JSON.parse("");
    // Begin Total
      getTotal();
      function getTotal() {
        $("#total").html('<div class="spinner spinner-track spinner-info mr-3"></div>');
        $.get("{{ route('employee_count') }}", function(response){
          $("#total").html('<span class="font-weight-bolder label label-xl label-light-info label-inline px-3 py-5 min-w-45px">'+response+'</span>');
        });
      }
    // End Total

    // Begin Datatable
    let dt_columns = [
      {
        field: 'id',
        title: 'id',
        sortable: 'desc',
        width: 30,
        type: 'number',
        selector: false,
        textAlign: 'center',
        
      },
      {
        field: 'name',
        title: 'Description',
        width:300,
        autoHide: false,
        template: function(row) {
          let description = '<br><table class="">';
          description += `<tr><td><b>NIP</b></td><td>&emsp;:&emsp;</td><td>${row.nip}</td></tr>`;
          description += `<tr><td><b>Name</b></td><td>&emsp;:&emsp;</td><td>${row.name}</td></tr>`;
          description += `<tr><td><b>Position</b></td><td>&emsp;:&emsp;</td><td>${row.role.name}</td></tr>`;
          description += `<tr><td><b>Email</b></td><td>&emsp;:&emsp;</td><td>${row.email}</td></tr>`;
          description += '</table><br>';
          return description;
        }
      },
      {
        field: 'actions',
        title: 'Actions',
        sortable: false,
        width: 200,
        textAlign: 'center',
        autoHide: false,
        template: function(row) {
          let action = '<br><table class="">';
          action += '\
          <tr>\
            <td>\
              <a href="#" class="menu-link px-3 btn-edit" data-id="'+row.id+'" data-nip="'+row.nip+'" data-name="'+row.name+'" data-email="'+row.email+'" data-role="'+row.role_id+'"> \
                <span class="menu-icon">\
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">\
                    <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z" fill="currentColor"></path>\
                    <path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z" fill="currentColor"></path>\
                    <path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z" fill="currentColor"></path>\
                  </svg>\
                </span>\
                Edit\
              </a>\
            </td>\
          </tr>\
          <tr><td></td></tr>\
          <tr><td></td></tr>\
          <tr>\
            <td>\
              <a href="#" class="menu-link px-3 btn-trash" data-id="' + row.id + '" data-title="' + row.name + '">\
                <span class="menu-icon">\
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">\
                    <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="currentColor"></path>\
                    <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="currentColor"></path>\
                    <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="currentColor"></path>\
                  </svg>\
                </span>\
                  Delete\
              </a>\
            </td>\
          </tr>\
          ';
          action += '</table><br>';
          // return action;
          return `<div class="d-flex flex-center">
                    ${action}
                  </div>`;
        }
      },
    ];
    let dt_all = $('#dt_all').KTDatatable({
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: "{{ route('employee_datatable') }}",
            params:{type:'all'},
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
        saveState: false,
      },
      // layout definition
      layout: {
        scroll: false,
        footer: false,
      },
      // column sorting
      sortable: true,
      pagination: true,
      search: {
        input: $('#dt_all_search_query'),
        key: 'generalSearch'
      },
      columns: dt_columns
    });

    // Remove dt_all Action 
    dt_columns.splice(-1);
    dt_columns.push({
        field: 'actions',
        title: 'Actions',
        sortable: false,
        width: 125,
        textAlign: 'center',
        autoHide: false,
        template: function(row) {
          let switch_status = {
            'draft': '',
            'publish': 'checked="checked"'
          };
          let enable_modify = "";
          let modify_btn = "";
          if(enable_modify){
            modify_btn = `
              <a href="courses/edit/${row.id}" class="btn btn-sm btn-clean btn-icon ml-2" data-id="${row.id}" data-title="${row.title}" title="Edit">
                <span class="svg-icon svg-icon-md">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <rect x="0" y="0" width="24" height="24"/>
                      <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "/>
                      <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"/>
                    </g>
                  </svg>
                </span>
              </a>
            `;
          }
          return `<div class="d-flex flex-center">
                    RESTORE
                  </div>
          `;
        }
      });
    
    let dt_trash = $('#dt_trash').KTDatatable({
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: "{{ route('employee_datatable') }}",
            params:{type:'trash'},
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 10,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
        saveState: false,
      },
      // layout definition
      layout: {
        scroll: false,
        footer: false,
      },
      // column sorting
      sortable: true,
      pagination: true,
      search: {
        input: $('#dt_trash_search_query'),
        key: 'generalSearch'
      },
      columns: dt_columns,
    });
    $('#dt_all_search_status').on('change', function() {
      dt_all.search($(this).val().toLowerCase(), 'status');
    });
    $('#dt_all_search_author').on('change', function() {
      dt_all.search($(this).val().toLowerCase(), 'author');
    });
    $('#dt_all_search_status').selectpicker();
    $("#dt_all_search_author").select2();
    // End Datatable

    // Begin Edit
    $("#dt_all").on("click",".btn-edit",function(evt){
        evt.preventDefault();
        $("#edit_id").val($(this).attr('data-id'));
        $("#data-nip").val($(this).attr('data-nip'));
        $("#data-name").val($(this).attr('data-name'));
        $("#data-email").val($(this).attr('data-email'));
        $("#data-role").val($(this).attr('data-role')).trigger('change');
      $("#modal_edit_employee").modal("show");
    }); 
    // End Edit

    $("#modal_edit_employee").submit(function(evt) {
      evt.preventDefault();
      let id = $("#edit_id").val();
      let action = "{{ route('employee_update', ['id' => ':id']) }}";
      action = action.replace(':id', id);
      let data   = $(this).serialize();
      
      $.ajax({
        url: action,
        type: 'PUT',
        data: data,
        success: function(response) {
          $('#modal_edit_employee').modal('hide')
          toastr.success(response.message);
          getTotal();
          dt_all.reload();
        }
      });
    });

    $("#form_edit_employee").on("submit", function(evt) {
        let id = $("#edit_id").val();
        let action = "{{ route('employee_update', ['id' => ':id']) }}";
        action = action.replace(':id', id);
        let data = $(this).serialize();

        $.ajax({
          url: action,
          type: 'PUT',
          data: data,
          success: function(response) {
            $("#modal_edit_employee").modal("hide");
            getTotal();
            dt_all.reload();
            toastr.success(response.message);
          }
        });
      });

    // Begin Move to Trash
      $("#dt_all").on("click",".btn-trash",function(evt){
        evt.preventDefault();
        let title = $(this).attr('data-title');
        let id = $(this).attr('data-id');
        let action = "{{ route('employee_delete', ['id' => ':id']) }}";
        action = action.replace(':id', id);

        Swal.fire({
          title: "Are you sure?",
          html: "Move to trash <strong>"+title+"</strong>",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, move to trash!"
        }).then(function(result) {
          if (result.value) {
            $.ajax({
              url: action,
              type: 'DELETE',
              data:{ type:'soft-delete' },
              success: function(response) {
                if(response.status == "success"){
                  getTotal();
                  dt_all.reload();
                  dt_trash.reload();
                }
                Swal.fire(
                  response.header,
                  response.message,
                  response.status
                );
              }
            });
          }
        });
      }); 
    // End Move to Trash

    // Begin Permanent Delete
    $("#dt_trash").on("click",".btn-delete",function(evt){
      evt.preventDefault();
      let title = $(this).attr('data-title');
      let id = $(this).attr('data-id');
      let action = "/app/events/"+id;

      Swal.fire({
        title: "Are you sure?",
        html: "Delete Event : <strong>"+title+"</strong>.<br> You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!"
      }).then(function(result) {
        if (result.value) {
          $.ajax({
            url: action,
            type: 'DELETE',
            data:{ type:'permanent-delete' },
            success: function(response) {
              getTotal();
              dt_all.reload();
              dt_trash.reload();
              Swal.fire(
                response.header,
                response.message,
                response.status
              );
            }
          });
        }
      });
    }); 
    // End Permanent Delete

    $('#modal_new_event').on('hidden.bs.modal', function () {
      $('#form_new_event')[0].reset()
      $('.type-wrapper').hide()
    })
    // Begin Submit New Form
    $(document).on('submit', '#form_new_event', function (event) {
      event.preventDefault()
      let action = "{{ route('employee_create') }}"
      let submitButton = $('.btn-save')
      // Do Ajax
      $.ajax({
        url: action,
        type: 'POST',
        data: $(this).serialize(),
        success: function (response) {
          $('#modal_new_event').modal('hide')
          toastr.success(response.message);
          getTotal();
          dt_all.reload();
        }
      })
    });

  });

    
    // End Submit New Form
</script>

@endsection