@extends('layouts.app-admin')
@section('content')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
      <div class="d-flex align-items-center flex-wrap mr-2">
        <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Data Reimbursements</h5>
        <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
        <div id="total"></div>
      </div>
      <div class="d-flex align-items-center">
        @if (Auth::user()->role_id == 3)
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal_new_reimbursement">
          New Reimbursement
        </button>
        @endif
      </div>
    </div>
  </div>
  <div class="d-flex flex-column-fluid">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card card-custom">
            <div class="card-header card-header-tabs-line">
              <div class="card-title">
              </div>
              <div class="card-toolbar">
                <ul class="nav nav-tabs nav-bold nav-light-danger nav-tabs-line">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab_all">
                      <span class="nav-icon"><i class="flaticon2-soft-icons"></i></span>
                      <span class="nav-text">All</span>
                    </a>
                  </li>
                  {{-- <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab_trash">
                      <span class="nav-icon"><i class="flaticon2-trash"></i></span>
                      <span class="nav-text">Trash</span>
                    </a>
                  </li> --}}
                </ul>
              </div>
            </div>
            <div class="card-body">
              <div class="tab-content">
                <div class="tab-pane fade show active" id="tab_all" role="tabpanel" aria-labelledby="tab_all">
                  <!--begin::Search Form-->
                  <div class="mb-7">
                    <div class="row align-items-center">
                      <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center justify-content-right">
                          <div class="col-md-4 my-2 my-md-0">
                            <div class="input-icon">
                              <input type="text" class="form-control" placeholder="Cari..."
                                id="dt_all_search_query" />
                              <span>
                                <i class="flaticon2-search-1 text-muted"></i>
                              </span>
                            </div>
                          </div>
                    

                        </div>
                      </div>
                    </div>
                  </div>
                  <!--end::Search Form-->
                  <div id="dt_all" class="datatable datatable-bordered datatable-head-custom"></div>
                </div>
                <div class="tab-pane fade" id="tab_trash" role="tabpanel" aria-labelledby="tab_trash">
                  <div class="d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                    <div class="d-flex align-items-center flex-wrap mr-2">
                      <div class="font-size-lg">Trashed Employee</div>
                    </div>
                    <div class="d-flex align-items-center">
                      <div class="font-size-lg mr-4">Filter</div>
                      <div class="input-icon">
                        <input type="text" class="form-control" placeholder="Search..." id="dt_trash_search_query">
                        <span>
                          <i class="flaticon2-search-1 text-muted"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div id="dt_trash" class="datatable datatable-bordered datatable-head-custom"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- Begin Modal New Reimbursement --}}
<div class="modal fade" id="modal_new_reimbursement" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form class="form" id="form_new_reimbursement" enctype="multipart/form-data">
        <div class="modal-header">
          <h5 class="modal-title">New Reimbursement</h5>
          <button type="button" class="btn btn-icon close" data-dismiss="modal" aria-label="Close">
            <x-svg-icon type="cross" />
          </button>
        </div>
        <div class="modal-body">
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">Date<span class="text-danger">*</span></label>
            <input type="date" class="form-control" name="date" id="date" placeholder="date" value="" required autofocus />
          </div>
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">Name<span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="" required />
          </div>
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">Description<span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="description" id="description" placeholder="description" value="" required />
          </div>
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">File</label>
            <input type="file" class="form-control" name="file" id="file" accept=".jpeg,.jpg,.png,.pdf" value="" />
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light-primary fw-bold" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary fw-bold" id="btn-save-new-employee">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- End Modal New Reimbursement --}}

{{-- Begin Modal Edit Reimbursement --}}
<div class="modal fade" id="modal_edit_reimbursement" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form class="form" id="form_edit_reimbursement">
        <div class="modal-header">
          <h5 class="modal-title">Edit Reimbursement</h5>
          <button type="button" class="btn btn-icon close" data-dismiss="modal" aria-label="Close">
            <x-svg-icon type="cross" />
          </button>
        </div>
        <div class="modal-body">
          <div class="fv-row mb-5">
            <input type="hidden" name="id" id="edit_id" />
            <label class="required fw-semibold fs-6 mb-2">Date<span class="text-danger">*</span></label>
            <input type="date" class="form-control" id="data-date" name="date" placeholder="date" value="" required autofocus />
          </div>
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">Name<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="data-name" name="name" placeholder="Name" value="" required />
          </div>
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">Description<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="data-description" name="description" placeholder="description" value="" required />
          </div>
          <div class="fv-row mb-5">
            <label class="required fw-semibold fs-6 mb-2">File</label>
            <input type="file" class="form-control" id="data-file" name="file" accept=".jpeg,.jpg,.png,.pdf" value="" />
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-light-primary fw-bold" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary fw-bold" id="btn-save-new-reimbursement">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- End Modal Edit Reimbursement --}}


  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script>
    $(document).ready(function(){
      let user_role_id = "{{ Auth::user()->role_id }}";
      // let arr_role_id = JSON.parse("");
      // Begin Total
        getTotal();
        function getTotal() {
          $("#total").html('<div class="spinner spinner-track spinner-info mr-3"></div>');
          $.get("{{ route('reimbursement_count') }}", function(response){
            $("#total").html('<span class="font-weight-bolder label label-xl label-light-info label-inline px-3 py-5 min-w-45px">'+response+' items</span>');
          });
        }
      // End Total

      // Begin Datatable
      let dt_columns = [
        {
          field: 'id',
          title: 'id',
          sortable: 'desc',
          width: 30,
          type: 'number',
          selector: false,
          textAlign: 'center',
          
        },
        {
          field: 'name',
          title: 'Name',
          width:200,
          autoHide: false,
          template: function(row) {
            let description = `<b>${row.name}</b><br>`;
            description += '<br><table class="">';
            description += `<tr><td><b>Date</b></td><td>&emsp;:&emsp;</td><td>${row.date}</td></tr>`;
            if (row.status == 'pending') {
              description += `<tr><td><b>Status</b></td><td>&emsp;:&emsp;</td><td>
                <span class="label label-warning label-pill label-inline mr-2">Pending</span>
                </td></tr>`;
              }
              if (row.status == 'approve') {
              description += `<tr><td><b>Status</b></td><td>&emsp;:&emsp;</td><td>
                <span class="label label-primary label-inline mr-2">Approve</span>
                </td></tr>`;
              
            }
            if (row.status == 'reject') {
              description += `<tr><td><b>Status</b></td><td>&emsp;:&emsp;</td><td>
                <span class="label label-danger label-pill label-inline mr-2">Reject</span>
                </td></tr>`;
              }
              if (row.file) {
                description += `<tr><td><b>File</b></td><td>&emsp;:&emsp;</td><td>
                    <a href="${row.file}" target="_blank" class="btn btn-text-dark-50 btn-icon-primary btn-hover-icon-success font-weight-bold btn-hover-bg-light mr-3" data-id="${row.id}">
                  <i class="flaticon2-graph-1"></i> Image</a>
                  </td></tr>`;
              }
            description += '</table><br>';
            return description;
          }
        },
        {
          field: 'description',
          title: 'Description',
          width:300,
          autoHide: false,
          template: function(row) {
            return row.description;
          }
        },
      ];

      let action = [{
          field: 'actions',
          title: 'Actions',
          sortable: false,
          width: 200,
          textAlign: 'center',
          autoHide: false,
          template: function(row) {
            let action = '<br><table class="">';
          action += '\
          <tr>\
            <td>\
              <a href="#" class="menu-link px-3 btn-edit" data-id="'+row.id+'" data-name="'+row.name+'" data-description="'+row.description+'" data-date="'+row.date+'" data-file="'+row.file+'"> \
                <span class="menu-icon">\
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">\
                    <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z" fill="currentColor"></path>\
                    <path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z" fill="currentColor"></path>\
                    <path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z" fill="currentColor"></path>\
                  </svg>\
                </span>\
                Edit\
              </a>\
            </td>\
          </tr>\
          <tr><td></td></tr>\
          <tr><td></td></tr>\
          <tr>\
            <td>\
              <a href="#" class="menu-link px-3 btn-trash" data-id="' + row.id + '" data-title="' + row.name + '">\
                <span class="menu-icon">\
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">\
                    <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="currentColor"></path>\
                    <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="currentColor"></path>\
                    <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="currentColor"></path>\
                  </svg>\
                </span>\
                  Delete\
              </a>\
            </td>\
          </tr>\
          ';
            action += '</table><br>';
            // return action;
            if (row.status == 'pending') {
              return `<div class="d-flex flex-center">
                        ${action}
                      </div>`;
            }
            return ``;
          }
        }];

      let submission = [{
          field: 'submission',
          title: 'Submission',
          sortable: false,
          width: 200,
          textAlign: 'center',
          autoHide: false,
          template: function(row) {
            let action = '<br><table class="">';
          action += '\
                <tr>\
                  <td>\
                    <a href="#" class="btn btn-primary font-weight-bold btn-pill btn-submission" data-id="' + row.id + '" data-status="approve">Approve</a>\
                  </td>\
                </tr>\
                <tr><td></td></tr>\
                <tr><td></td></tr>\
                <tr>\
                  <td>\
                    <a href="#" class="btn btn-danger font-weight-bold btn-pill btn-submission" data-id="' + row.id + '" data-status="reject">Reject</a>\
                  </td>\
                </tr>\
                ';
            action += '</table><br>';
            // return action;
            if (row.status == 'pending') {
                return `<div class="d-flex flex-center">
                          ${action}
                        </div>`;
            }
            return ``;
          }
        }];

        if (user_role_id == 1 || user_role_id == 2) {
          dt_columns = dt_columns.concat(submission);
        }

        if (user_role_id == 3) {
          dt_columns = dt_columns.concat(action);
        }

        let dt_all = $('#dt_all').KTDatatable({
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'GET',
              url: "{{ route('reimbursement_datatable') }}",
              params:{type:'all'},
              map: function(raw) {
                // sample data mapping
                var dataSet = raw;
                if (typeof raw.data !== 'undefined') {
                  dataSet = raw.data;
                }
                return dataSet;
              },
            },
          },
          pageSize: 10,
          serverPaging: true,
          serverFiltering: true,
          serverSorting: true,
          saveState: false,
        },
        // layout definition
        layout: {
          scroll: false,
          footer: false,
        },
        // column sorting
        sortable: true,
        pagination: true,
        search: {
          input: $('#dt_all_search_query'),
          key: 'generalSearch'
        },
        columns: dt_columns
      });

      $('#dt_all_search_status').on('change', function() {
        dt_all.search($(this).val().toLowerCase(), 'status');
      });
      $('#dt_all_search_author').on('change', function() {
        dt_all.search($(this).val().toLowerCase(), 'author');
      });
      $('#dt_all_search_status').selectpicker();
      $("#dt_all_search_author").select2();
      // End Datatable

    // Begin Edit
      $("#dt_all").on("click",".btn-edit",function(evt){
        evt.preventDefault();
        $("#edit_id").val($(this).attr('data-id'));
        $("#data-date").val($(this).attr('data-date'));
        $("#data-name").val($(this).attr('data-name'));
        $("#data-description").val($(this).attr('data-description'));
        // $("#data-file").val($(this).attr('data-file'));
        $("#modal_edit_reimbursement").modal("show");
      }); 
    // End Edit

    $("#form_edit_reimbursement").submit(function(evt) {
      evt.preventDefault();
      let id = $("#edit_id").val();
      let action = "{{ route('reimbursement_update', ['id' => ':id']) }}";
      action = action.replace(':id', id);

      let formData = new FormData();
      formData.append('date', $('#data-date').val());
      formData.append('name', $('#data-name').val());
      formData.append('description', $('#data-description').val());
      formData.append('file', $('#data-file').prop('files')[0]);

      let data   = $(this).serialize();
      
      $.ajax({
        url: action,
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        success: function(response) {
          $('#modal_edit_reimbursement').modal('hide')
          toastr.success(response.message);
          getTotal();
          dt_all.reload();
        }
      });
  });

    // Begin Move to Trash
    $("#dt_all").on("click",".btn-trash",function(evt){
        evt.preventDefault();
        let title = $(this).attr('data-title');
        let id = $(this).attr('data-id');
        let action = "{{ route('reimbursement_delete', ['id' => ':id']) }}";
        action = action.replace(':id', id);

        Swal.fire({
          title: "Are you sure?",
          html: "Move to trash <strong>"+title+"</strong>",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, move to trash!"
        }).then(function(result) {
          if (result.value) {
            $.ajax({
              url: action,
              type: 'DELETE',
              data:{ type:'soft-delete' },
              success: function(response) {
                if(response.status == "success"){
                  getTotal();
                  dt_all.reload();
                  dt_trash.reload();
                }
                Swal.fire(
                  response.header,
                  response.message,
                  response.status
                );
              }
            });
          }
        });
      }); 
    // End Move to Trash

    // Begin Move to Trash
    $("#dt_all").on("click",".btn-submission",function(evt){
        evt.preventDefault();
        let title = $(this).attr('data-title');
        let id = $(this).attr('data-id');
        let status = $(this).attr('data-status');
        let action = "{{ route('reimbursement_submission', ['id' => ':id', 'status' => ':status']) }}";
        action = action.replace(':id', id);
        action = action.replace(':status', status);

        Swal.fire({
          title: "Are you sure?",
          html: "Change submission <strong>"+status+"</strong>",
          icon: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, change to <strong>"+status+"</strong>",
        }).then(function(result) {
          if (result.value) {
            $.ajax({
              url: action,
              type: 'PATCH',
              data:{ type:'soft-delete' },
              success: function(response) {
                if(response.status == "success"){
                  getTotal();
                  dt_all.reload();
                  dt_trash.reload();
                }
                Swal.fire(
                  response.header,
                  response.message,
                  response.status
                );
              }
            });
          }
        });
      }); 
    // End Move to Trash

    $('#modal_new_reimbursement').on('hidden.bs.modal', function () {
      $('#form_new_reimbursement')[0].reset()
      $('.type-wrapper').hide()
    })

    // Begin Submit New Form
    $(document).on('submit', '#form_new_reimbursement', function (event) {
      event.preventDefault()
      let action = "{{ route('reimbursement_create') }}"
      let submitButton = $('.btn-save')
      let formData = new FormData();
      formData.append('date', $('#date').val());
      formData.append('name', $('#name').val());
      formData.append('description', $('#description').val());
      formData.append('file', $('#file').prop('files')[0]);
      // Do Ajax
      $.ajax({
        url: action,
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        success: function (response) {
          $('#modal_new_reimbursement').modal('hide')
          toastr.success(response.message);
          getTotal();
          dt_all.reload();
        }
      })
    })
    });
  </script>

@endsection