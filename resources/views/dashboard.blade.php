@extends('layouts.app-admin')
@section('content')
    <!--begin::Content-->
								<!--begin::Row-->
								<div class="d-flex flex-column-fluid">
									<div class="container">
								<div class="row">
									<div class="col-lg-4">
										<!--begin::Mixed Widget 14-->
										<div class="card card-custom card-stretch gutter-b">
											<!--begin::Header-->
											<div class="card-header border-0 pt-5">
												<h3 class="card-title font-weight-bolder">Hi {{ Auth::user()->name }}, You login role {{ Auth::user()->role->name }}</h3>
											</div>
											<!--end::Header-->
											<!--begin::Body-->
						
											<!--end::Body-->
										</div>
										<!--end::Mixed Widget 14-->
									</div>
								</div>
							</div>
								<!--end::Row-->
								<!--end::Dashboard-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!--end::Content-->
@endsection